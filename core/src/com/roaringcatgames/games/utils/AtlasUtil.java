package com.roaringcatgames.games.utils;

/**
 * Created with IntelliJ IDEA.
 * User: barry
 * Date: 1/24/15
 * Time: 10:00 AM
 */
public class AtlasUtil {

    public static final String BG_MOUNTAINS = "bg/BG1";
    public static final String BG_FOREST = "bg/BG2";
    public static final String BG_CLOUDS = "bg/PolyCloud";
    public static final String BG_FIELD = "bg/Green";
    public static final String BG_PINE = "bg/Pine";
    public static final String BG_TREE = "bg/Tree";
    public static final String BG_MOON = "bg/Moon";
    public static final String FG_TREES = "bg/BG3";

    public static final String GO_CAMPING = "intro/Go_Camping";
    public static final String GO_CAMPING_TILT = "intro/Go_Camping_Tilt";

    public static String WHEEL = "car/Wheel";
    public static String CAR_BODY = "car/Wagon";
    public static String CAR_STILL = "car/Wagon_Still";

    public static String CAMP_TENT = "camp/Tent";
    public static String CAMP_TRASHCAN = "camp/trashcan";
    public static String CAMP_RIVER = "camp/River";
    public static String CAMP_FIRE = "camp/Fire";
    public static String CAMP_LIGHT = "camp/light";
    public static String CAMP_TENT_O = "camp/Tent_Lit";
    public static String CAMP_SIGN = "camp/Post";

    public static String CAMPER_CROUCH = "camper/Camper_Crouch";
    public static String CAMPER_STAND = "camper/camper_stand";
    public static String CAMPER_WALK = "camper/Camper_Walk";
    public static String CAMPER_SURPRISED = "camper/Camper_Surprised";
    public static String CAMPER_CARRY = "camper/Camper_Carry";
    public static String CAMPER_CARRY_STAND = "camper/Camper_Carry_Stand";
    public static String BUBBLE_FIRE = "camper/Bubble_Fire";
    public static String BUBBLE_CAN = "camper/Bubble_Bin";
    public static String BUBBLE_RIVER = "camper/Bubble_River";


    public static String RANGER_WALKING = "ranger/Ranger_Walk";
    public static String RANGER_THREATEN = "ranger/Ranger_Wag";

    public static String SS_RUN = "ss/Sassy_Run";
    public static String SS_POSE = "ss/Sassy_Pose";
    public static String SS_SAMSQUANTCH = "ss/Sassy_Party";

    public static String TRASH_BEER = "trash/Can";
    public static String TRASH_PAPER = "trash/Candy";
    public static String TRASH_TIRE = "trash/Tire";

}
