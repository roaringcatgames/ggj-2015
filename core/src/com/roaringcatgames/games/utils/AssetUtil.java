package com.roaringcatgames.games.utils;

/**
 * Created with IntelliJ IDEA.
 * User: barry
 * Date: 1/24/15
 * Time: 9:51 AM
 */
public class AssetUtil {

    public static String ANI_ATLAS = "animations/animations.atlas";
    public static String REXLIA_48 = "fonts/rexlia-48.fnt";

    public static String TITLE_SOUND2 = "sounds/intro_title2.mp3";
    public static String CAR_DRIVING_SOUND = "sounds/intro_car_driving.mp3";
    public static String HIT_WOOSH1_SOUND = "sounds/hit_woosh1.mp3";
    public static String HIT_WOOSH2_SOUND = "sounds/hit_woosh2.mp3";
    public static String FIRE_CRACKLE_SOUND = "sounds/fire_crackle.mp3";
    public static String WATER_DROPLET_SOUND = "sounds/water_droplet.mp3";
    public static String WIND2_SOUND = "sounds/wind2.mp3";
    public static String FIRE_GROW_SOUND = "sounds/fire_throw_grow.mp3";
    public static String SASQUATCH_YUB_SOUND = "sounds/sasquach_yub_yub.mp3";
    public static String RANGER_WARNING_SOUND = "sounds/ranger_warning_dialog.mp3";
    public static String RANGER_EYE_SOUND = "sounds/ranger_eye_on_you.mp3";
    public static String RANGER_GET_OUT2_SOUND = "sounds/ranger_get_out_park2.mp3";
    public static String MOON_FART = "sounds/moon_fart.mp3";
    public static String LOGO_IMG = "title.png";//"logo.png";
}
