package com.roaringcatgames.games.scene2d.Actors;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.kasetagen.engine.gdx.scenes.scene2d.actors.GenericActor;

/**
 * Created with IntelliJ IDEA.
 * User: barry
 * Date: 1/25/15
 * Time: 1:52 PM
 */
public class Trash extends GenericActor {

    private GenericActor target;
    private boolean hasBeenHeld = false;

    public Trash(float x, float y, float width, float height, TextureRegion textureRegion, GenericActor target) {
        super(x, y, width, height, textureRegion, null);

        this.target = target;
    }

    public GenericActor getTarget(){
        return target;
    }

    public void setHasBeenHeld(boolean hasBeenHeld){
        this.hasBeenHeld = hasBeenHeld;
    }

    public boolean hasBeenHeld(){
        return hasBeenHeld;
    }
}
