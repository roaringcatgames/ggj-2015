package com.roaringcatgames.games.scene2d.Actors;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.kasetagen.engine.gdx.scenes.scene2d.actors.AnimatedActor;

/**
 * Created with IntelliJ IDEA.
 * User: barry
 * Date: 1/24/15
 * Time: 3:44 PM
 * To change this template use File | Settings | File Templates.
 */
public class DirectionalAnimatedActor extends AnimatedActor {
    public boolean isLookingLeft = false;
    public boolean animationsAreLeftFacing = false;

    public DirectionalAnimatedActor(float x, float y, float width, float height, Animation animation, float startKeyframe, boolean animationsAreLeft) {
        super(x, y, width, height, animation, startKeyframe);
        animationsAreLeftFacing = animationsAreLeft;
        adjustTextureRegions();
    }

    public void switchDirections(){
        isLookingLeft = !isLookingLeft;
        adjustTextureRegions();
    }

    private void adjustTextureRegions() {
        boolean shouldBeFlipped;
        if(animationsAreLeftFacing){
            if(isLookingLeft){
                shouldBeFlipped = false;
            }else{
                shouldBeFlipped = true;
            }
        }else{
            if(isLookingLeft){
                shouldBeFlipped = true;
            }else{
                shouldBeFlipped = false;
            }
        }

        for(Animation ani:stateAnimations.values()){
            for(TextureRegion tr:ani.getKeyFrames()){
                if((tr.isFlipX() && !shouldBeFlipped) ||
                   (!tr.isFlipX() && shouldBeFlipped)){
                    tr.flip(true, false);
                }
            }
        }
    }

    @Override
    public void addStateAnimation(String state, Animation ani) {
        super.addStateAnimation(state, ani);
        adjustTextureRegions();
    }
}
