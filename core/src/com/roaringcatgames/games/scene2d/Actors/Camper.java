package com.roaringcatgames.games.scene2d.Actors;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.kasetagen.engine.gdx.scenes.scene2d.actors.GenericActor;

/**
 * Created with IntelliJ IDEA.
 * User: barry
 * Date: 1/24/15
 * Time: 7:14 PM
 */
public class Camper extends DirectionalAnimatedActor {

    private GenericActor heldItem;
    private Rectangle pickupTrigger;

    private float itemInset = 55f;

    public Camper(float x, float y, float width, float height, Animation animation, float startKeyframe, boolean animationsAreLeft) {
        super(x, y, width, height, animation, startKeyframe, animationsAreLeft);

        pickupTrigger = new Rectangle(x, y, width/3f, width/3f);
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        if(isLookingLeft){
            pickupTrigger.setPosition(getX(), getY());
            if(heldItem != null){
                heldItem.setPosition(getX()+itemInset-heldItem.getWidth(), getHeight()/2 + getY());
            }
        }else{
            pickupTrigger.setPosition(getRight()-pickupTrigger.width, getY());
            if(heldItem != null){
                heldItem.setPosition(getRight()-itemInset, getHeight()/2 + getY());
            }
        }

    }

    public boolean canPickup(Rectangle target){

        return heldItem == null && pickupTrigger.overlaps(target);
    }

    public void setHeldItem(GenericActor a){
        heldItem = a;
    }

    public void dropItem(float targetY){
        if(heldItem != null){
            heldItem.addAction(Actions.moveTo(heldItem.getX()+(velocity.x/2), targetY, 0.5f));
            heldItem = null;
        }
    }

    public boolean hasItem(){
        return heldItem != null;
    }
}
