package com.roaringcatgames.games.scene2d;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;
import com.kasetagen.engine.IGameProcessor;
import com.kasetagen.engine.gdx.scenes.scene2d.ActorDecorator;
import com.kasetagen.engine.gdx.scenes.scene2d.Kitten2dStage;
import com.kasetagen.engine.gdx.scenes.scene2d.actors.AnimatedActor;
import com.kasetagen.engine.gdx.scenes.scene2d.actors.GenericActor;
import com.kasetagen.engine.gdx.scenes.scene2d.actors.GenericGroup;
import com.kasetagen.engine.gdx.scenes.scene2d.decorators.ShakeDecorator;
import com.roaringcatgames.games.PartySquatchGame;
import com.roaringcatgames.games.scene2d.Decorators.InfiniteLeftDecorator;
import com.roaringcatgames.games.scene2d.Decorators.InfiniteTileDecorator;
import com.roaringcatgames.games.utils.AssetUtil;
import com.roaringcatgames.games.utils.AtlasUtil;
import com.roaringcatgames.games.utils.TypesUtil;

import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: barry
 * Date: 1/24/15
 * Time: 11:14 AM
 */
public class IntroStage extends Kitten2dStage {
    private GenericGroup bgEnvironment;
    private GenericGroup fgEnvironment;

    //private TextButton startGameButton;
    private ImageButton startGameButton;

    private Sound titleSound;
    private Sound carSound;

    private float ROAD_OFFSET_X = -26f;
    private float ROAD_OFFSET_Y = -20f;
    private static float FG_Y = -50f;

    private float MIDGROUND_Y = -50f;

    private GenericGroup car;
    private static float CAR_WIDTH = 500f;
    private static float CAR_HEIGHT = 132f;
    private static float WHEEL_1_X = 110f;
    private static float WHEEL_2_X = 365f;
    private static float WHEEL_SIZE = 65f;

//    private static float RIVER_WIDTH = 1024f;
//    private static float RIVER_HEIGHT = 768f;
//    private static float RIVER_X = 1760f;
//    private static float RIVER_Y = -60f;
//    private static float RIVER_CYCLE_RATE = 1f/8f;

    private static Random rand;

    private ActorDecorator infiniteLeftDecorator;
    private ActorDecorator infiniteTileDecorator;
    private ActorDecorator roadTileDecorator;

    public IntroStage(IGameProcessor gp) {
        super(gp);

        rand = new Random(System.currentTimeMillis());
        AssetManager am = gameProcessor.getAssetManager();
        TextureAtlas atlas = am.get(AssetUtil.ANI_ATLAS, TypesUtil.TEXTURE_ATLAS);

        //Sounds
        titleSound = am.get(AssetUtil.TITLE_SOUND2, TypesUtil.SOUND);

        carSound = am.get(AssetUtil.CAR_DRIVING_SOUND, TypesUtil.SOUND);
        carSound.loop(1.0f);

        //Build background
        bgEnvironment = new GenericGroup(0, 0, getWidth(), getHeight(), null, Color.BLUE);
        addActor(bgEnvironment);

        infiniteLeftDecorator = new InfiniteLeftDecorator(getWidth());

        infiniteTileDecorator = new InfiniteTileDecorator();
        roadTileDecorator = new InfiniteTileDecorator(ROAD_OFFSET_X, 0f, 2);


        //Add Clouds
        Array<TextureAtlas.AtlasRegion> clouds = atlas.findRegions(AtlasUtil.BG_CLOUDS);
        Array<GenericActor> cloudActors = new Array<GenericActor>();

        float w = clouds.get(0).getRegionWidth();
        float h = clouds.get(0).getRegionHeight();
        int openSpots = (int)(getWidth()/w);

        //Clouds behind mountains
        for(int i=0;i<=4;i++){
            float xPos = i * (w * 1.25f);
            float yPos = getHeight() - (i * (h/2));
            int index = rand.nextInt(clouds.size);
            GenericActor c = new GenericActor(xPos, yPos, w, h, clouds.get(index), Color.BLACK);
            c.addDecorator(infiniteLeftDecorator);
            c.velocity.x = -50f;
            cloudActors.add(c);
            bgEnvironment.addActor(c);
        }

        //Add Mountains
        Array<TextureAtlas.AtlasRegion> mountains = atlas.findRegions(AtlasUtil.BG_MOUNTAINS);
        Array<GenericActor> mountainActors = new Array<GenericActor>();
        tileActor(0f, 0f, 20f, mountains, mountainActors, 0f, infiniteTileDecorator);

        Animation sassyPose = new Animation(1f, atlas.findRegions(AtlasUtil.SS_POSE));
        AnimatedActor sassy = new AnimatedActor(1480f, 250f,200f, 200f, sassyPose, 0f);
        sassy.velocity.x = -40f;
        sassy.addDecorator(infiniteLeftDecorator);
        bgEnvironment.addActor(sassy);

        Array<TextureAtlas.AtlasRegion> forests = atlas.findRegions(AtlasUtil.BG_FOREST);
        Array<GenericActor> forestActors = new Array<GenericActor>();
        tileActor(0f, MIDGROUND_Y, 40f, forests, forestActors, 0f, infiniteTileDecorator);

//        Animation riverAni = new Animation(RIVER_CYCLE_RATE, atlas.findRegions(AtlasUtil.CAMP_RIVER));
//        AnimatedActor river = new AnimatedActor(RIVER_X, RIVER_Y, RIVER_WIDTH, RIVER_HEIGHT, riverAni, 0f);
//        river.addDecorator(infiniteLeftDecorator);
//        river.velocity.x = -20f;
//        bgEnvironment.addActor(river);


        //Clouds in front of Mountains
        for(int i=0;i<=3;i++){
            float xPos = i * (w * 1.25f);
            float yPos = (getHeight() - (h*1.5f)) + (i * (h/2));
            int index = rand.nextInt(clouds.size);
            GenericActor c = new GenericActor(xPos, yPos, w, h, clouds.get(index), Color.BLACK);
            c.addDecorator(infiniteLeftDecorator);
            c.velocity.x = -75f;
            cloudActors.add(c);
            bgEnvironment.addActor(c);
        }



        //Build Road
        Array<TextureAtlas.AtlasRegion> roads = atlas.findRegions("intro/Road");
        roads.get(0).flip(false, true);
        Array<GenericActor> roadActors = new Array<GenericActor>();
        tileActor(ROAD_OFFSET_X, ROAD_OFFSET_Y, 200f, roads, roadActors, ROAD_OFFSET_X, roadTileDecorator);

        //Build Car
        //TextureRegion sky = am.get(AssetUtil.SKY_BG, AssetUtil.TEXTURE);
        car = new GenericGroup(getWidth()/2 - CAR_WIDTH/2, 0f, CAR_WIDTH, CAR_HEIGHT, null, Color.BLUE);
        car.addActor(new AnimatedActor(0f, 0f, CAR_WIDTH, CAR_HEIGHT, null, 0f));

        TextureRegion wheelRegion = atlas.findRegion(AtlasUtil.WHEEL);
        GenericActor backWheel = new GenericActor(WHEEL_1_X, 0f, WHEEL_SIZE, WHEEL_SIZE, wheelRegion, Color.BLACK);
        GenericActor frontWheel = new GenericActor(WHEEL_2_X, 0f, WHEEL_SIZE, WHEEL_SIZE, wheelRegion, Color.BLACK);

        ActorDecorator wheelRotator = new ActorDecorator() {
            private float speed = -180f; //30degrees per second
            @Override
            public void applyAdjustment(Actor actor, float v) {
                actor.setRotation(actor.getRotation() + (speed*v));
            }
        };

        TextureRegion bodyRegion = atlas.findRegion(AtlasUtil.CAR_BODY);
        GenericActor body = new GenericActor(0f, 0f, CAR_WIDTH, CAR_HEIGHT, bodyRegion, Color.RED);
        body.addDecorator(new ShakeDecorator(10f, 20f, 1f, 0.3f));

        backWheel.addDecorator(wheelRotator);
        frontWheel.addDecorator(wheelRotator);
        car.addActor(backWheel);
        car.addActor(frontWheel);
        car.addActor(body);

        bgEnvironment.addActor(car);

        //Build foreground

        //Build Title
        ClickListener listener = new ClickListener()
        {
            @Override
            public void clicked(InputEvent event, float x, float y)
            {
                Actor btn = event.getListenerActor();

                if(btn == startGameButton){
                    car.velocity.x += 260f;
                    titleSound.play(1.0f);
                }
            }
        };


//        TextButton.TextButtonStyle style = new TextButton.TextButtonStyle();
//        style.font = gameProcessor.getAssetManager().get(AssetUtil.REXLIA_48, TypesUtil.BITMAP_FONT);
//        style.fontColor =  Color.CYAN;
//        style.overFontColor = Color.RED;
//        style.downFontColor = Color.GRAY;

        startGameButton = new ImageButton(new TextureRegionDrawable(atlas.findRegion(AtlasUtil.GO_CAMPING)),
                                          new TextureRegionDrawable(atlas.findRegion(AtlasUtil.GO_CAMPING_TILT)));

        //startGameButton = new TextButton("Touch to Start", style);
        startGameButton.addListener(listener);
        startGameButton.setSize(500f, 225f);
        startGameButton.setPosition(getWidth() / 2 - startGameButton.getWidth() / 2, (getHeight() / 2) - (startGameButton.getHeight()/2));

        addActor(startGameButton);

        GenericActor logoActor = new GenericActor(0, 0, 558, 118, new TextureRegion(am.get(AssetUtil.LOGO_IMG, TypesUtil.TEXTURE)), Color.WHITE);
        logoActor.setPosition(getWidth()/2 - logoActor.getWidth()/2, getHeight()/2+150);
        addActor(logoActor);

        GenericActor fgTrees = new GenericActor(0f, FG_Y, 3000f, 720f, atlas.findRegion(AtlasUtil.FG_TREES), Color.GREEN);
        fgTrees.addDecorator(infiniteLeftDecorator);
        fgTrees.velocity.x = -200f;
        bgEnvironment.addActor(fgTrees);
    }

    protected void tileActor(float x, float y, float velocity,
                             Array<TextureAtlas.AtlasRegion> regionArray,
                             Array<GenericActor> actorArray,
                             float xOffset, ActorDecorator tileDecorator){


        for(int i=regionArray.size -1; i >= 0;i--){
            TextureAtlas.AtlasRegion region = regionArray.get(i);
            GenericActor actor1 = new GenericActor(x, y, region.getRegionWidth(), region.getRegionHeight(), region, Color.WHITE);
            GenericActor actor2 = new GenericActor(actor1.getX() + region.getRegionWidth() + xOffset, y, region.getRegionWidth(), region.getRegionHeight(), region, Color.WHITE);
            actor1.velocity.x = velocity/(float)-(i+1);
            actor2.velocity.x = velocity/(float)-(i+1);
            actor1.addDecorator(tileDecorator);
            actor2.addDecorator(tileDecorator);
            actorArray.add(actor1);
            actorArray.add(actor2);
            bgEnvironment.addActor(actor1);
            bgEnvironment.addActor(actor2);
        }
    }

    @Override
    public void act(float delta){
        super.act(delta);

        if(car.getX() > getWidth()) {
            addAction(Actions.sequence(
                    Actions.fadeOut(2f),
                    Actions.run(new Runnable() {
                        @Override
                        public void run() {
                            gameProcessor.changeToScreen(PartySquatchGame.GAME);
                            carSound.stop();
                        }
                    })));

        }
    }

    @Override
    public boolean keyDown(int keyCode) {
        if(keyCode == Input.Keys.SPACE){
            if(car.velocity.x == 0f){
                car.velocity.x += 260f;
                titleSound.play(1.0f);
            }
        }
        return super.keyDown(keyCode);
    }
}
