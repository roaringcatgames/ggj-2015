package com.roaringcatgames.games.scene2d;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.kasetagen.engine.IGameProcessor;
import com.kasetagen.engine.gdx.scenes.scene2d.StagedKitten2dScreen;

/**
 * Created with IntelliJ IDEA.
 * User: barry
 * Date: 1/24/15
 * Time: 9:08 AM
 */
public class IntroScreen extends StagedKitten2dScreen{

    public IntroScreen(IGameProcessor delegate) {
        super(delegate);
    }

    @Override
    protected Stage createStage() {
        return new IntroStage(gameProcessor);
    }

    @Override
    protected void initialize() {

    }
}
