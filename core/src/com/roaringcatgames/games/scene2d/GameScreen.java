package com.roaringcatgames.games.scene2d;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.kasetagen.engine.IGameProcessor;
import com.kasetagen.engine.gdx.scenes.scene2d.StagedKitten2dScreen;

/**
 * Created with IntelliJ IDEA.
 * User: barry
 * Date: 1/24/15
 * Time: 1:25 AM
 */
public class GameScreen extends StagedKitten2dScreen {
    public GameScreen(IGameProcessor delegate) {
        super(delegate);
    }

    @Override
    protected Stage createStage() {
        return new GameStage(this.gameProcessor);
    }

    @Override
    protected void initialize() {

    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        ((GameStage)stage).resize(width, height);
    }
}
