package com.roaringcatgames.games.scene2d;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.kasetagen.engine.IGameProcessor;
import com.kasetagen.engine.gdx.scenes.scene2d.ActorDecorator;
import com.kasetagen.engine.gdx.scenes.scene2d.Kitten2dStage;
import com.kasetagen.engine.gdx.scenes.scene2d.actors.GenericActor;
import com.kasetagen.engine.util.MathUtil;
import com.roaringcatgames.games.utils.AssetUtil;
import com.roaringcatgames.games.utils.AtlasUtil;
import com.roaringcatgames.games.utils.TypesUtil;

/**
 * Created by Mike on 1/25/2015.
 */
public class WinStage extends Kitten2dStage {
    private FrameBuffer fbo;
    private ShaderProgram finalShader;
    private ShaderProgram defaultShader;
    private TextureRegion lightRegion;

    public static final float ambientIntensity = .7f;
    public static final Vector3 ambientColor = new Vector3(0.3f, 0.3f, 0.7f);
    private static final String vertexShader = "attribute vec4 a_position;\n" +
            "attribute vec4 a_color;\n" +
            "attribute vec2 a_texCoord0;\n" +
            "uniform mat4 u_projTrans;\n" +
            "varying vec4 vColor;\n" +
            "varying vec2 vTexCoord;\n" +
            "\n" +
            "void main() {\n" +
            "\tvColor = a_color;\n" +
            "\tvTexCoord = a_texCoord0;\n" +
            "\tgl_Position = u_projTrans * a_position;\t\t\n" +
            "}";

    private static final String defaultPixelShader = "#ifdef GL_ES\n" +
            "#define LOWP lowp\n" +
            "precision mediump float;\n" +
            "#else\n" +
            "#define LOWP\n" +
            "#endif\n" +
            "\n" +
            "varying LOWP vec4 vColor;\n" +
            "varying vec2 vTexCoord;\n" +
            "\n" +
            "//our texture samplers\n" +
            "uniform sampler2D u_texture; //diffuse map\n" +
            "\n" +
            "void main() {\n" +
            "\tvec4 DiffuseColor = texture2D(u_texture, vTexCoord);\n" +
            "\tgl_FragColor = vColor * DiffuseColor;\n" +
            "}";

    private static final String finalPixelShader = "#ifdef GL_ES\n" +
            "#define LOWP lowp\n" +
            "precision mediump float;\n" +
            "#else\n" +
            "#define LOWP\n" +
            "#endif\n" +
            "\n" +
            "varying LOWP vec4 vColor;\n" +
            "varying vec2 vTexCoord;\n" +
            "\n" +
            "//texture samplers\n" +
            "uniform sampler2D u_texture; //diffuse map\n" +
            "uniform sampler2D u_lightmap;   //light map\n" +
            "\n" +
            "//additional parameters for the shader\n" +
            "uniform vec2 resolution; //resolution of screen\n" +
            "uniform LOWP vec4 ambientColor; //ambient RGB, alpha channel is intensity \n" +
            "\n" +
            "void main() {\n" +
            "\tvec4 diffuseColor = texture2D(u_texture, vTexCoord);\n" +
            "\tvec2 lighCoord = (gl_FragCoord.xy / resolution.xy);\n" +
            "\tvec4 light = texture2D(u_lightmap, lighCoord);\n" +
            "\t\n" +
            "\tvec3 ambient = ambientColor.rgb * ambientColor.a;\n" +
            "\tvec3 intensity = ambient + light.rgb;\n" +
            " \tvec3 finalColor = diffuseColor.rgb * intensity;\n" +
            "\t\n" +
            "\tgl_FragColor = vColor * vec4(finalColor, diffuseColor.a);\n" +
            "}\n";

    //Text
    Label winLabel;

    public WinStage(IGameProcessor gp) {
        super(gp);
        Gdx.app.log("WIN SCREEN", "YOU WIN FOR REAL!!!");
        shaderStuff();

        Label.LabelStyle style = new Label.LabelStyle();
        style.font = gameProcessor.getAssetManager().get(AssetUtil.REXLIA_48, TypesUtil.BITMAP_FONT);
        style.fontColor =  Color.CYAN;

        winLabel = new Label("YOU WIN!!!", style);
        winLabel.setPosition(getWidth()/2 - winLabel.getWidth()/2, getHeight()/2 - winLabel.getHeight()/2);
        addActor(winLabel);
    }

    public void resize(int width, int height) {
        fbo = new FrameBuffer(Pixmap.Format.RGBA8888, width, height, false);

        getViewport().update(width, height);
        finalShader.begin();
        finalShader.setUniformf("resolution", width, height);
        finalShader.end();
    }

    private void shaderStuff(){
        ShaderProgram.pedantic = false;

        defaultShader = new ShaderProgram(vertexShader, defaultPixelShader);

        finalShader = new ShaderProgram(vertexShader, finalPixelShader);
        finalShader.begin();
        finalShader.setUniformi("u_lightmap", 1);
        finalShader.setUniformf("ambientColor", ambientColor.x, ambientColor.y,
                ambientColor.z, ambientIntensity);
        finalShader.end();

        TextureAtlas atlas = gameProcessor.getAssetManager().get(AssetUtil.ANI_ATLAS, TypesUtil.TEXTURE_ATLAS);
        lightRegion = atlas.findRegion(AtlasUtil.CAMP_LIGHT);
    }
}
