package com.roaringcatgames.games.scene2d;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.kasetagen.engine.IGameProcessor;
import com.kasetagen.engine.gdx.scenes.scene2d.StagedKitten2dScreen;

/**
 * Created by Mike on 1/25/2015.
 */
public class LoseScreen extends StagedKitten2dScreen {
    public LoseScreen(IGameProcessor delegate) {
        super(delegate);
    }

    @Override
    protected Stage createStage() {
        return new LoseStage(this.gameProcessor);
    }

    @Override
    protected void initialize() {

    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        ((LoseStage) stage).resize(width, height);
    }
}