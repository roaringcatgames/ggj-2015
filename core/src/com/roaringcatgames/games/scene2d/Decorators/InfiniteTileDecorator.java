package com.roaringcatgames.games.scene2d.Decorators;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.kasetagen.engine.gdx.scenes.scene2d.ActorDecorator;
import com.kasetagen.engine.gdx.scenes.scene2d.actors.GenericActor;

/**
 * Created with IntelliJ IDEA.
 * User: barry
 * Date: 1/25/15
 * Time: 2:25 PM
 */
public class InfiniteTileDecorator implements ActorDecorator {
    private float xOffset = 0f;
    private float yOffset = 0f;
    private int numberOfTiles = 0;

    public InfiniteTileDecorator(){
        numberOfTiles = 2;
    }
    public InfiniteTileDecorator(float xOffset, float yOffset, int tileCount){
        this.xOffset = xOffset;
        this.yOffset = yOffset;
        numberOfTiles = tileCount;
    }

    @Override
    public void applyAdjustment(Actor actor, float v) {
        GenericActor ga = ((GenericActor)actor);
        if(ga.getX()+ga.getWidth() < 0){
            ga.setPosition(ga.getX() + (ga.getWidth()+xOffset)*(numberOfTiles), actor.getY() + yOffset);
        }
    }
}
