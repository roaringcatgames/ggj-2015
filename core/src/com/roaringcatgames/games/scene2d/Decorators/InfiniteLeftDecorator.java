package com.roaringcatgames.games.scene2d.Decorators;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.kasetagen.engine.gdx.scenes.scene2d.ActorDecorator;
import com.kasetagen.engine.gdx.scenes.scene2d.actors.GenericActor;

/**
 * Created with IntelliJ IDEA.
 * User: barry
 * Date: 1/25/15
 * Time: 2:26 PM
 */
public class InfiniteLeftDecorator implements ActorDecorator {

    private float rightSide;
    public InfiniteLeftDecorator(float rightSide){
        this.rightSide = rightSide;
    }

    @Override
    public void applyAdjustment(Actor actor, float v) {
        GenericActor ga = ((GenericActor)actor);
        if(ga.getX()+ga.getWidth() < 0f){
            ga.setPosition(rightSide + ga.getWidth(), actor.getY());
        }
    }
}
