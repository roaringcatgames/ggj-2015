package com.roaringcatgames.games.scene2d;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.kasetagen.engine.IGameProcessor;
import com.kasetagen.engine.gdx.scenes.scene2d.*;
import com.kasetagen.engine.gdx.scenes.scene2d.actors.AnimatedActor;
import com.kasetagen.engine.gdx.scenes.scene2d.actors.GenericActor;
import com.kasetagen.engine.gdx.scenes.scene2d.actors.GenericGroup;
import com.kasetagen.engine.gdx.scenes.scene2d.cameraMods.ActorFollowerCameraMod;
import com.kasetagen.engine.gdx.scenes.scene2d.decorators.OffsetColliderDecorator;
import com.kasetagen.engine.gdx.scenes.scene2d.decorators.ParticleSpawnerDecorator;
import com.roaringcatgames.games.PartySquatchGame;
import com.roaringcatgames.games.scene2d.Actors.Camper;
import com.roaringcatgames.games.scene2d.Actors.DirectionalAnimatedActor;
import com.roaringcatgames.games.scene2d.Actors.Trash;
import com.roaringcatgames.games.scene2d.Decorators.InfiniteLeftDecorator;
import com.roaringcatgames.games.utils.AssetUtil;
import com.roaringcatgames.games.utils.AtlasUtil;
import com.roaringcatgames.games.utils.TypesUtil;

import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: barry
 * Date: 1/24/15
 * Time: 11:29 AM

 */
public class GameStage extends Kitten2dStage {

    private boolean controlsEnabled = false;

    //Shaders
    private static boolean isNightTime = false;

    //Camera
    private static float CAMERA_LEFT_BOUND = 1000f;
    private static float CAMERA_RIGHT_BOUND =2340f;
    private static float CAMERA_TRIGGER = 300f;
    private static ActorFollowerCameraMod cameraFollower;

    private static float MOON_X = 950f;
    private static float MOON_WIDTH = 200f;
    private static float MOON_HEIGHT = 200f;
    private GenericActor moon;


    //Car
    private static float CAR_WIDTH = 750f;
    private static float CAR_HEIGHT = 198f;
    private static float CAR_X = 0f;

    //Tent
    private static float TENT_WIDTH = 400f;
    private static float TENT_HEIGHT = 200f;
    private static float TENT_X = 800f;
    private AnimatedActor tent;

    //Fire
    private static float FIRE_WIDTH = 150f;
    private static float FIRE_HEIGHT = 150f;
    private static float FIRE_X = 1400f;
    private static float FIRE_CYCLE_RATE = 1f/6f;
    private AnimatedActor fire;
    private GenericActor fireAmbience;

    //Camper
    private static Camper camper;
    private static float CAMPER_WIDTH = 300f;
    private static float CAMPER_HEIGHT = 300f;
    private static float CAMPER_X = 1100f;
    private static float CAMPER_SPEED = 400f;
    private static float CAMPER_CYCLE_RATE = 1f/6f;


    //Ranger
    private static DirectionalAnimatedActor ranger;
    private static float RANGER_WIDTH = 300f;
    private static float RANGER_HEIGHT = 300f;
    private static float RANGER_X = -200f;
    private static float RANGER_CYCLE_RATE = 1f/6f;


    //Trash
    private static float TRASH_WIDTH = 50f;
    private static float TRASH_HEIGHT = 50f;
    private static Array<Trash> trash;
    private static IActorDisposer trashDisposer;
    private static IActorSpawner<Trash> trashSpawner;

    private static GenericActor trashCan;
    private static float TCAN_WIDTH = 125f;
    private static float TCAN_HEIGHT = 125f;
    private static float TCAN_X = TENT_X-50f;

    private static float PAPER_WIDTH = 50f;
    private static float PAPER_HEIGHT = 50f;
    private static float TIRE_WIDTH = 150f;
    private static float TIRE_HEIGHT = 150f;

    //Env
    private GenericGroup bgEnv;
    private GenericGroup playArea;
    private GenericGroup fgEnv;
    private static AnimatedActor river;
    private static float RIVER_WIDTH = 1024f;
    private static float RIVER_HEIGHT = 768f;
    private static float RIVER_X = 1810f;
    private static float RIVER_Y = -120f;
    private static float RIVER_CYCLE_RATE = 1f/3f;
    private static float FG_Y = -50f;
    private static InfiniteLeftDecorator infiniteLeftDecorator;

    //Sounds
    private static Sound trash1Sound;
    private static Sound trash2Sound;
    private static Sound fireCrackleSound;
    private static Sound waterDropletSound;
    private static Sound wind2Sound;
    private static Sound fireSound;
    private static Sound rangerWarningSound;
    private static Sound sasYubSound;
    private static Sound rangerEyeSound;
    private static Sound rangerGetOut2Sound;
    private static Sound moonFartSound;

    //Sasquatch
    private DirectionalAnimatedActor sasquatch;
    private static float SS_WIDTH = 500f;
    private static float SS_HEIGHT = 500f;
    private static float SS_X = 3000f;
    private static float SS_CYCLE_RATE = 1f/8f;
    private ParticleSpawnerDecorator trashParticleGenerator;

    //Timer
    private static float secondsToRanger = 0f;
    private int nightCount = 1;

    //Text
    Label timerLabel;

    //Trees
    private static Array<GenericActor> trees;
    private static float[] treeXPositions = new float[] {  10f, 600f, 400f, 1650f, 1500f, 2700f, 2800f};
    private static float[] treeYPositions = new float[] { 100f, 110f, 100f,  115f,  100f,  110f,  100f};
    private static float TREE_WIDTH =  300f;
    private static float TREE_HEIGHT = 600f;

    private static float LEFT_BOUNDS = 400f;
    private static float RIGHT_BOUNDS = RIVER_X + (RIVER_WIDTH*(1f/3f));//3000f;
    private static float LOWER_BOUNDS = 100f;
    private static float UPPER_BOUNDS = 600f;

    private static Random rand = new Random(System.currentTimeMillis());

    private AnimatedActor infoBubble;
    private static float BUBBLE_WIDTH = 175f;
    private static float BUBBLE_HEIGHT = 250f;
    private static float BUBBLE_CYCLE_RATE = 1f/8f;
    private static float POST_X = 575f;
    private static float POST_W = 150f;
    private static float POST_H = 150f;


    private boolean hasShownFireBubble = false;
    private boolean hasShownCanBubble = false;
    private boolean hasShownRiverBubble = false;

    public GameStage(IGameProcessor gp) {
        super(gp);

        Label.LabelStyle style = new Label.LabelStyle();
        style.font = gameProcessor.getAssetManager().get(AssetUtil.REXLIA_48, TypesUtil.BITMAP_FONT);
        style.fontColor =  Color.CYAN;

        timerLabel = new Label("", style);

        //Add Sounds
        trash1Sound = gp.getAssetManager().get(AssetUtil.HIT_WOOSH1_SOUND, TypesUtil.SOUND);
        trash2Sound = gp.getAssetManager().get(AssetUtil.HIT_WOOSH2_SOUND, TypesUtil.SOUND);
        fireCrackleSound = gp.getAssetManager().get(AssetUtil.FIRE_CRACKLE_SOUND, TypesUtil.SOUND);
        waterDropletSound = gp.getAssetManager().get(AssetUtil.WATER_DROPLET_SOUND, TypesUtil.SOUND);
        wind2Sound = gp.getAssetManager().get(AssetUtil.WIND2_SOUND, TypesUtil.SOUND);
        sasYubSound = gp.getAssetManager().get(AssetUtil.SASQUATCH_YUB_SOUND, TypesUtil.SOUND);
        fireSound = gp.getAssetManager().get(AssetUtil.FIRE_GROW_SOUND, TypesUtil.SOUND);
        rangerWarningSound = gp.getAssetManager().get(AssetUtil.RANGER_WARNING_SOUND, TypesUtil.SOUND);
        rangerEyeSound = gp.getAssetManager().get(AssetUtil.RANGER_EYE_SOUND, TypesUtil.SOUND);
        rangerGetOut2Sound = gp.getAssetManager().get(AssetUtil.RANGER_GET_OUT2_SOUND, TypesUtil.SOUND);
        moonFartSound = gp.getAssetManager().get(AssetUtil.MOON_FART, TypesUtil.SOUND);

        wind2Sound.loop(0.8f);
        fireCrackleSound.loop(0.8f);

        TextureAtlas atlas = gp.getAssetManager().get(AssetUtil.ANI_ATLAS, TypesUtil.TEXTURE_ATLAS);

        infiniteLeftDecorator = new InfiniteLeftDecorator(RIGHT_BOUNDS);

        bgEnv = new GenericGroup(0f, 0f, 3000f, 720f, null, Color.BLACK);
        playArea = new GenericGroup(0f, 0f, 3000f, 720f, null, Color.BLACK);
        fgEnv = new GenericGroup(0f, 0f, 3000f, 720f, null, Color.BLACK);
        addActor(bgEnv);
        addActor(playArea);
        addActor(fgEnv);

        //Add Clouds
        Array<TextureAtlas.AtlasRegion> clouds = atlas.findRegions(AtlasUtil.BG_CLOUDS);
        Array<GenericActor> cloudActors = new Array<GenericActor>();

        float w = clouds.get(0).getRegionWidth();
        float h = clouds.get(0).getRegionHeight();

        //Clouds behind mountains
        for(int i=0;i<=5;i++){
            float xPos = i * (w * 1.25f);
            float yPos = getHeight() - (i%3 * (h/2));
            int index = rand.nextInt(clouds.size);
            GenericActor c = new GenericActor(xPos, yPos, w, h, clouds.get(index), Color.BLACK);
            c.addDecorator(infiniteLeftDecorator);
            c.velocity.x = -50f;
            cloudActors.add(c);
            bgEnv.addActor(c);
        }


        moon = new GenericActor(MOON_X, 0f, MOON_WIDTH, MOON_HEIGHT, atlas.findRegion(AtlasUtil.BG_MOON), null);
        bgEnv.addActor(moon);

        GenericActor bg = new GenericActor(0f, 0f, 3000f, 720f, atlas.findRegion(AtlasUtil.BG_MOUNTAINS), Color.WHITE);
        bgEnv.addActor(bg);
        GenericActor forest = new GenericActor(0f, 0f, 3000f, 720f, atlas.findRegion(AtlasUtil.BG_FOREST), Color.WHITE);
        bgEnv.addActor(forest);

        Animation riverAni = new Animation(RIVER_CYCLE_RATE, atlas.findRegions(AtlasUtil.CAMP_RIVER));
        river = new AnimatedActor(RIVER_X, RIVER_Y, RIVER_WIDTH, RIVER_HEIGHT, riverAni, 0f);
        bgEnv.addActor(river);
        river.addDecorator(new OffsetColliderDecorator(RIVER_WIDTH*(1f/3f), 0f, RIVER_WIDTH*(2f/3f), RIVER_HEIGHT/3f));

        //Clouds in front of Mountains
        for(int i=0;i<=4;i++){
            float xPos = i * (w * 1.25f);
            float yPos = (getHeight() - (h*1.5f)) + ((i%3) * (h/2));
            int index = rand.nextInt(clouds.size);
            GenericActor c = new GenericActor(xPos, yPos, w, h, clouds.get(index), Color.BLACK);
            c.addDecorator(infiniteLeftDecorator);
            c.velocity.x = -75f;
            cloudActors.add(c);
            bgEnv.addActor(c);
        }

        //Add Trees
        trees = new Array<GenericActor>();
        for(int i=0;i<treeXPositions.length;i++){
            Array<TextureAtlas.AtlasRegion> pines = atlas.findRegions(AtlasUtil.BG_PINE);
            int treeOption = rand.nextInt(pines.size);
            GenericActor ga = new GenericActor(treeXPositions[i], treeYPositions[i], TREE_WIDTH, TREE_HEIGHT, pines.get(treeOption), null);
            trees.add(ga);
            bgEnv.addActor(ga);
        }

        //Add Car
        TextureRegion bodyRegion = atlas.findRegion(AtlasUtil.CAR_STILL);
        GenericActor car = new GenericActor(CAR_X, LOWER_BOUNDS, CAR_WIDTH, CAR_HEIGHT, bodyRegion, Color.RED);
        car.flipTextureRegion(true, false);
        bgEnv.addActor(car);

        TextureRegion controlsSign = atlas.findRegion(AtlasUtil.CAMP_SIGN);
        GenericActor post = new GenericActor(POST_X, LOWER_BOUNDS, POST_W, POST_H, controlsSign, Color.BLUE);
        bgEnv.addActor(post);



        Animation tentAnimation = new Animation(1f/2f, atlas.findRegions(AtlasUtil.CAMP_TENT));
        tent = new AnimatedActor(TENT_X, LOWER_BOUNDS, TENT_WIDTH, TENT_HEIGHT, tentAnimation, 0f);
        Animation tentOccupiedAni = new Animation(1f/2f, atlas.findRegions(AtlasUtil.CAMP_TENT_O));
        tent.addStateAnimation("OCCUPIED", tentOccupiedAni);
        tent.flipTextureRegion(true, false);
        bgEnv.addActor(tent);

        Animation fireAni = new Animation(FIRE_CYCLE_RATE, atlas.findRegions(AtlasUtil.CAMP_FIRE));
        fire = new AnimatedActor(FIRE_X, LOWER_BOUNDS, FIRE_WIDTH, FIRE_HEIGHT, fireAni, 0f);
        bgEnv.addActor(fire);


        trashCan = new GenericActor(TCAN_X, LOWER_BOUNDS, TCAN_WIDTH, TCAN_HEIGHT, atlas.findRegion(AtlasUtil.CAMP_TRASHCAN), Color.BLACK);
        bgEnv.addActor(trashCan);




        Animation playerStandingAni = new Animation(CAMPER_CYCLE_RATE*2, atlas.findRegions(AtlasUtil.CAMPER_STAND));
        camper = new Camper(CAMPER_X, LOWER_BOUNDS, CAMPER_WIDTH, CAMPER_HEIGHT, playerStandingAni, 0f, false);
        camper.addStateAnimation("STANDING", playerStandingAni);
        Animation playerWalkingAni = new Animation(CAMPER_CYCLE_RATE, atlas.findRegions(AtlasUtil.CAMPER_WALK));
        camper.addStateAnimation("WALKING", playerWalkingAni);
        Animation playerSurprisedAni = new Animation(CAMPER_CYCLE_RATE, atlas.findRegions(AtlasUtil.CAMPER_SURPRISED));
        camper.addStateAnimation("SURPRISED", playerSurprisedAni);
        Animation playerCarryAni = new Animation(CAMPER_CYCLE_RATE, atlas.findRegions(AtlasUtil.CAMPER_CARRY));
        camper.addStateAnimation("CARRYING", playerCarryAni);
        Animation playerCarryStand = new Animation(CAMPER_CYCLE_RATE, atlas.findRegions(AtlasUtil.CAMPER_CARRY_STAND));
        camper.addStateAnimation("CARRYSTAND", playerCarryStand);
        Animation playerCrouchingAni = new Animation(CAMPER_CYCLE_RATE/3f, atlas.findRegions(AtlasUtil.CAMPER_CROUCH));
        camper.addStateAnimation("CROUCHING", playerCrouchingAni);
        camper.switchDirections();
        playArea.addActor(camper);

        infoBubble = new AnimatedActor(CAMPER_X, LOWER_BOUNDS+CAMPER_HEIGHT, BUBBLE_WIDTH, BUBBLE_HEIGHT, null, 0f);
        Animation fireAnimation = new Animation(BUBBLE_CYCLE_RATE, atlas.findRegions(AtlasUtil.BUBBLE_FIRE));
        infoBubble.addStateAnimation("FIRE", fireAnimation);
        Animation canAnimation = new Animation(BUBBLE_CYCLE_RATE, atlas.findRegions(AtlasUtil.BUBBLE_CAN));
        infoBubble.addStateAnimation("CAN", canAnimation);
        Animation riverAnimation = new Animation(BUBBLE_CYCLE_RATE, atlas.findRegions(AtlasUtil.BUBBLE_RIVER));
        infoBubble.addStateAnimation("RIVER", riverAnimation);
        infoBubble.setIsLooping(false);
        infoBubble.setVisible(false);
        infoBubble.addDecorator(new ActorDecorator() {
            private GenericActor target = camper;
            @Override
            public void applyAdjustment(Actor actor, float v) {
                actor.setX(target.getX()+actor.getWidth());
            }
        });
        playArea.addActor(infoBubble);

        Animation rangerWalkingAnimation = new Animation(RANGER_CYCLE_RATE, atlas.findRegions(AtlasUtil.RANGER_WALKING), Animation.PlayMode.LOOP);
        ranger = new DirectionalAnimatedActor(RANGER_X, LOWER_BOUNDS, RANGER_WIDTH, RANGER_HEIGHT, rangerWalkingAnimation, 0f, true);
        Animation rangerThreatenAni = new Animation(RANGER_CYCLE_RATE, atlas.findRegions(AtlasUtil.RANGER_THREATEN));
        ranger.addStateAnimation("THREATENING", rangerThreatenAni);
        ranger.switchDirections();
        playArea.addActor(ranger);

        addCameraMod(new ICameraModifier() {
            private float targetPos;
            private float speed = 100f;
            private boolean isComplete = false;
            private boolean isInitialized = false;

            @Override
            public void modify(Camera camera, float v) {
                if(!isInitialized){
                    isInitialized = true;
                    targetPos = camera.position.x + 400f;
                }
                camera.position.x += speed*v;
                if(camera.position.x >= targetPos){
                    isComplete = true;
                    runRangerIntroSequence(ranger, camper);
                }
            }

            @Override
            public boolean isComplete() {
                return isComplete;
            }
        });

        cameraFollower = new ActorFollowerCameraMod(camper, CAMERA_TRIGGER, CAMERA_LEFT_BOUND, CAMERA_RIGHT_BOUND);
        addCameraMod(cameraFollower);

        trash = new Array<Trash>();
        trashDisposer = new IActorDisposer(){
            @Override
            public void dispose(Actor actor) {
                trash.removeValue((Trash)actor, true);
            }
        };

        trashSpawner = new IActorSpawner<Trash>() {
            @Override
            public Trash spawn() {
                Trash t = null;
                if(sasquatch.getX() > LEFT_BOUNDS){

                    TextureAtlas atlas = gameProcessor.getAssetManager().get(AssetUtil.ANI_ATLAS, TypesUtil.TEXTURE_ATLAS);
                    int targetIndex = rand.nextInt(10);
                    GenericActor target;

                    float w, h;
                    TextureRegion region;
                    if(targetIndex < 5){
                        w = TRASH_WIDTH;
                        h = TRASH_HEIGHT;
                        region = atlas.findRegion(AtlasUtil.TRASH_BEER);
                        target = trashCan;
                    }else if(targetIndex < 8){
                        w = PAPER_WIDTH;
                        h = PAPER_HEIGHT;
                        region = atlas.findRegion(AtlasUtil.TRASH_PAPER);
                        target = fire;
                    }else{
                        w = TIRE_WIDTH;
                        h = TIRE_HEIGHT;
                        region = atlas.findRegion(AtlasUtil.TRASH_TIRE);
                        target = river;
                    }

                    t = new Trash(0f, 0f, w, h, region, target);
                    t.setDisposer(trashDisposer);
                    t.setRotationSpeed(540f);
                    t.addDecorator(new ActorDecorator() {
                        float elapsedTime = 0f;
                        boolean isComplete = false;

                        @Override
                        public void applyAdjustment(Actor actor, float v) {
                            if (!isComplete) {
                                elapsedTime += v;

                                if (elapsedTime > 1f) {
                                    ((GenericActor) actor).setRotationSpeed(0f);
                                    isComplete = true;
                                }
                            }
                        }
                    });
                    trash.add(t);
                }
                return t;
            }
        };



        Animation ssAni = new Animation(SS_CYCLE_RATE, atlas.findRegions(AtlasUtil.SS_SAMSQUANTCH));
        sasquatch = new DirectionalAnimatedActor(SS_X, LOWER_BOUNDS, SS_WIDTH, SS_HEIGHT, ssAni, 0f, true);
        Animation ssRunAni = new Animation(SS_CYCLE_RATE, atlas.findRegions(AtlasUtil.SS_RUN));
        sasquatch.addStateAnimation("RUNNING", ssRunAni);
        sasquatch.switchDirections();
        playArea.addActor(sasquatch);

        float frequency = 0.8f;
        float xRange = 20f;
        float yRange = 0f;
        Vector2 leftBounds = new Vector2(LEFT_BOUNDS, LOWER_BOUNDS);
        //Vector2 rightBounds = new Vector2(RIGHT_BOUNDS-river.getWidth(), UPPER_BOUNDS);
        Vector2 rightBounds = new Vector2(river.getX(), UPPER_BOUNDS);
        trashParticleGenerator = new ParticleSpawnerDecorator(frequency, xRange, yRange, leftBounds, rightBounds, trashSpawner);
        trashParticleGenerator.setTargetY(LOWER_BOUNDS, true);

        sharderStuff();

        fgEnv.addActor(timerLabel);

        GenericActor fgTrees = new GenericActor(0f, FG_Y, 3000f, 720f, atlas.findRegion(AtlasUtil.FG_TREES), Color.GREEN);
        fgEnv.addActor(fgTrees);
    }

    private void setControlsEnabled(boolean enabled){
        controlsEnabled = enabled;
    }

    private float RANGER_WALK_TIME = 2f;
    private float RANGER_TALK_TIME = 7f;//7f;

    private void runRangerIntroSequence(final AnimatedActor rangerRef, final AnimatedActor camperRef) {
        if(ranger.isLookingLeft)
            ranger.switchDirections();

        rangerRef.addAction(Actions.sequence(
                Actions.moveTo(CAMPER_X - rangerRef.getWidth(), 0f, RANGER_WALK_TIME),
                Actions.run(new Runnable() {
                    @Override
                    public void run() {
                        camperRef.setState("STANDING", true);
                        rangerRef.setState("THREATENING", true);
                        rangerWarningSound.play(1.0f);
                    }
                }),
                Actions.delay(RANGER_TALK_TIME),
                Actions.run(new Runnable() {
                    @Override
                    public void run() {
                        rangerRef.setState("DEFAULT", true);
                        ranger.switchDirections();

                    }
                }),
                Actions.moveTo(RANGER_X, 0f, RANGER_WALK_TIME),
                Actions.run(new Runnable() {
                    @Override
                    public void run() {
                        runSasquatchSequence(nightCount);
//                        camperRef.setState("DEFAULT", true);
//                        for(int i=0;i<3;i++){
//                            Trash t = trashSpawner.spawn();
//                            t.setPosition(TENT_X + (100f * (i+1f)), 0f);
//                            addActor(t);
//                        }
//                        startTimer(1);
//                        setControlsEnabled(true);
//                        cameraFollower.setEnabled(controlsEnabled);
                    }
                })
        ));
    }


    private void startTimer(int day){

        switch(day){
            case 1:
                secondsToRanger = 20f;
                trashParticleGenerator.setFrequency(0.8f);
                break;
            case 2:

                secondsToRanger = 25f;
                trashParticleGenerator.setFrequency(0.6f);
                break;
            case 3:

                secondsToRanger = 30f;
                trashParticleGenerator.setFrequency(0.4f);
                break;
            case 4:
                secondsToRanger = 31f;
                trashParticleGenerator.setFrequency(0.35f);
                break;
            case 5:

                secondsToRanger = 32f;
                trashParticleGenerator.setFrequency(0.2f);
                break;

            default:
                secondsToRanger = 33f;
                break;

        }
    }

    private void runWinEndSequence(){
        Gdx.app.log("WIN CONDITION", "YOU WIN!!!");
        setControlsEnabled(false);
        fireCrackleSound.stop();
        wind2Sound.stop();
        gameProcessor.changeToScreen(PartySquatchGame.WIN);
    }

    private void runLoseEndSequence(){
        Gdx.app.log("LOSE CONDITION", "YOU LOSE!!!");
        setControlsEnabled(false);
        camper.velocity.x = 0f;
        //Send Player to Tent
        isNightTime = false;

        if(ranger.isLookingLeft)
            ranger.switchDirections();

        if(camper.getX() > tent.getRight()){
            if(!camper.isLookingLeft){
                camper.switchDirections();
            }
        }
        else {
            if (camper.isLookingLeft) {
                camper.switchDirections();
            }
        }

        ranger.addAction(Actions.sequence(Actions.moveTo(CAMPER_X - ranger.getWidth(), LOWER_BOUNDS, RANGER_WALK_TIME), Actions.run(new Runnable() {
            @Override
            public void run() {
                camper.setState("WALKING", true);

                camper.addAction(Actions.sequence(Actions.moveTo(tent.getRight(), camper.getY(), 4f), Actions.run(new Runnable() {
                    @Override
                    public void run() {
                        if (!camper.isLookingLeft) {
                            camper.switchDirections();
                        }
                        camper.setState("STANDING", true);
                        ranger.setState("THREATENING", true);
                        rangerGetOut2Sound.play(1.0f);
                    }
                }), Actions.delay(2f), Actions.run(new Runnable() {
                    @Override
                    public void run() {
                        fireCrackleSound.stop();
                        wind2Sound.stop();
                        gameProcessor.changeToScreen(PartySquatchGame.LOSE);
                    }
                })));
            }
        })));
    }

    private void runWinSequence() {
        Gdx.app.log("WIN", "YOU WIN THIS ROUND!!!");
        setControlsEnabled(false);
        camper.velocity.x = 0f;
        //Send Player to Tent
        isNightTime = false;

        if(ranger.isLookingLeft)
            ranger.switchDirections();

        if(camper.getX() > tent.getRight()){
            if(!camper.isLookingLeft){
                camper.switchDirections();
            }
        }
        else{
            if(camper.isLookingLeft){
                camper.switchDirections();
            }
        }

        camper.setState("WALKING", true);

        camper.addAction(Actions.sequence(Actions.moveTo(tent.getRight(), camper.getY(), 4f), Actions.run(new Runnable() {
            @Override
            public void run() {
                ranger.addAction(Actions.sequence(
                        Actions.moveTo(CAMPER_X - ranger.getWidth(), LOWER_BOUNDS, RANGER_WALK_TIME),
                        Actions.run(new Runnable() {
                            @Override
                            public void run() {
                                if(!camper.isLookingLeft){
                                    camper.switchDirections();
                                }
                                camper.setState("STANDING", true);
                                ranger.setState("THREATENING", true);
                                rangerEyeSound.play(1.0f);
                            }
                        }),
                        Actions.delay(2f),
                        Actions.run(new Runnable() {
                            @Override
                            public void run() {
                                ranger.setState("DEFAULT", true);
                                ranger.switchDirections();
                            }
                        }),
                        Actions.moveTo(RANGER_X, LOWER_BOUNDS, RANGER_WALK_TIME),
                        Actions.run(new Runnable() {
                            @Override
                            public void run() {
                                runSasquatchSequence(nightCount);
                            }
                        })
                ));
            }
        })));
    }

    private void runSasquatchSequence(int night){
        setControlsEnabled(false);

        SequenceAction seq = Actions.sequence();
        //Send Player to Tent
        seq.addAction(Actions.run(new Runnable() {
            @Override
            public void run() {
                camper.velocity.x = 0f;
                if((camper.isLookingLeft && camper.getX() < tent.getRight()) ||
                        (!camper.isLookingLeft && camper.getX() > tent.getRight())){
                    camper.switchDirections();
                }
            }
        }));

        //if(nightCount != 1)
        //    seq.addAction(Actions.moveTo(tent.getRight(), camper.getY(), 4f));

        seq.addAction(Actions.run(new Runnable() {
            @Override
            public void run() {
                camper.setVisible(false);
                tent.setState("OCCUPIED", false);
            }
        }));

        //Move camera to the right
        seq.addAction(Actions.run(new Runnable() {
            @Override
            public void run() {

                Gdx.app.log("RUNNABLE", "Adding Camera Mod");
                cameraFollower.setEnabled(false);

                //Move Camera to the far right
                addCameraMod(new ICameraModifier() {
                    private boolean isComplete = false;
                    private float speed = 300f;

                    @Override
                    public void modify(Camera camera, float v) {
                        if (camera.position.x < CAMERA_RIGHT_BOUND) {
                            camera.position.x += speed * v;
                        } else {
                            isComplete = true;
                            isNightTime = true;

                            moon.addAction(
                                    Actions.moveTo(MOON_X, getHeight() - (MOON_HEIGHT * 1.1f), 4f, Interpolation.elasticOut));

                            //Spawn SAMSQUANCH

                            sasquatch.addDecorator(trashParticleGenerator);
                            sasquatch.addAction(Actions.parallel(Actions.run(new Runnable() {
                                @Override
                                public void run() {
                                    sasYubSound.play(1.0f);
                                    sasquatch.setState("RUNNING", true);
                                }
                            })));
                            sasquatch.addAction(Actions.sequence(
                                    Actions.moveTo(2000f, LOWER_BOUNDS, 1f),
                                    Actions.run(new Runnable() {
                                        @Override
                                        public void run() {
                                            sasquatch.setState("DEFAULT", true);
                                        }
                                    }),
                                    Actions.moveTo(0f, LOWER_BOUNDS, 3f),
                                    Actions.run(new Runnable() {
                                        @Override
                                        public void run() {
                                            sasquatch.removeDecorator(trashParticleGenerator);
                                        }
                                    }),
                                    Actions.moveTo(SS_X, LOWER_BOUNDS)));

                            //Move Camera to the left
                            addCameraMod(new ICameraModifier() {
                                boolean isComplete = false;
                                boolean isInitialized = false;
                                float speed = 200f;


                                @Override
                                public void modify(Camera innerCamera, float v) {
                                    if (!isInitialized) {
                                        isInitialized = true;
                                        innerCamera.position.x = CAMERA_RIGHT_BOUND;
                                    } else {
                                        if (innerCamera.position.x > CAMERA_LEFT_BOUND) {
                                            innerCamera.position.x -= this.speed * v;
                                        } else {
                                            this.isComplete = true;

                                            camper.addAction(Actions.run(new Runnable() {
                                                @Override
                                                public void run() {
                                                    camper.setState("SURPRISED", true);
                                                    //startTimer(1);
                                                    setControlsEnabled(true);
                                                    cameraFollower.setEnabled(controlsEnabled);
                                                    moon.addAction(Actions.sequence(
                                                            Actions.rotateBy(20f, 0.5f),
                                                            Actions.run(new Runnable() {
                                                                @Override
                                                                public void run() {
                                                                    moonFartSound.play(1f);
                                                                }
                                                            }),
                                                            Actions.rotateBy(-20f, 0.5f),
                                                            Actions.moveTo(MOON_X, 0f, 1f)
                                                            ));
                                                    isNightTime = false;

                                                    startTimer(nightCount);
                                                    nightCount++;
                                                    camper.setVisible(true);
                                                    tent.setState("DEFAULT", true);
                                                }
                                            }));
                                        }
                                    }
                                }

                                @Override
                                public boolean isComplete() {
                                    return this.isComplete;
                                }
                            });
                        }
                    }

                    @Override
                    public boolean isComplete() {
                        return isComplete;
                    }
                });
            }
        }));


        camper.addAction(seq);
    }


    @Override
    public void act(float delta) {
        super.act(delta);

        if(controlsEnabled) {
            timerLabel.setPosition(getCamera().position.x + 260, getCamera().position.y + 300);
            timerLabel.setVisible(true);
            timerLabel.setText("Time: "+(int)secondsToRanger);
            if (camper.getX() < LEFT_BOUNDS) {
                camper.setPosition(LEFT_BOUNDS, camper.getY());
            } else if (camper.getRight() > RIGHT_BOUNDS) {
                camper.setPosition(RIGHT_BOUNDS - camper.getWidth(), camper.getY());
            }

            if(camper.getY() < LOWER_BOUNDS){
                camper.setPosition(camper.getX(), LOWER_BOUNDS);
            }else if(camper.getTop() > UPPER_BOUNDS){
                camper.setPosition(camper.getX(), UPPER_BOUNDS-camper.getHeight());
            }


            if(camper.getCurrentState().equals("CROUCHING") && !camper.isAnimationComplete()){
                //Still doing this.
            }else{
                camper.setIsLooping(true);
                if(camper.velocity.x != 0){
                    if(camper.hasItem()){
                        camper.setState("CARRYING", false);
                    }else{
                        camper.setState("WALKING", false);
                    }
                }else{
                    if(camper.hasItem()){
                        camper.setState("CARRYSTAND", false);
                    }else if(camper.getCurrentState() == "SURPRISED"){
                        camper.setState("SURPRISED", false);
                    }else{
                        camper.setState("STANDING", false);
                    }
                }
            }

            for(GenericActor tree:trees){
                if(camper.canPickup(tree.collider)){
                    if((Gdx.input.isKeyPressed(Input.Keys.W) || Gdx.input.isKeyPressed(Input.Keys.UP))){
                        camper.velocity.y = CAMPER_SPEED;
                    }else if(Gdx.input.isKeyPressed(Input.Keys.S) || Gdx.input.isKeyPressed(Input.Keys.DOWN)){
                        camper.velocity.y = -CAMPER_SPEED;
                    }else{
                        camper.velocity.y = 0;
                    }
                }
            }

            if (trash.size < 1) {
                if(nightCount < 5) {
                    runWinSequence();
                }
                else{
                    runWinEndSequence();
                }
            } else {
                for (Trash t : trash) {
                    if (t.getTarget() == trashCan && t.hasBeenHeld() && t.collider.overlaps(trashCan.collider)) {
                        Random rand = new Random();

                        if (rand.nextInt(2) == 0)
                            trash1Sound.play(1.0f);
                        else
                            trash2Sound.play(1.0f);

                        t.setIsRemovable(true);
                    } else if (t.getTarget() == river && t.hasBeenHeld() && t.collider.overlaps(river.collider)) {
                        waterDropletSound.play(1.0f);

                        t.setIsRemovable(true);
                    }else if(t.getTarget() == fire && t.hasBeenHeld() && t.collider.overlaps(fire.collider)){
                        fireSound.play(1.0f);
                        t.setIsRemovable(true);
                    }
                }
            }

            secondsToRanger -= delta;

            // LOSE Condition
            if (secondsToRanger <= 0) {
                controlsEnabled = false;
                camper.velocity.x = 0f;
                runLoseEndSequence();
            }
        } else{
            timerLabel.setVisible(false);
        }
    }

    /**
     *
     * Input Events
     *
     **/
    @Override
    public boolean keyDown(int keyCode) {
        if(!controlsEnabled){
            return false;
        }

        if(camper.getY() == LOWER_BOUNDS){
            if(Input.Keys.LEFT == keyCode || Input.Keys.A == keyCode){
                if(!camper.isLookingLeft){
                    camper.switchDirections();
                }
                camper.velocity.x = -CAMPER_SPEED;
                if(camper.hasItem()){
                    camper.setState("CARRYING", false);
                }else{
                    camper.setState("WALKING", false);
                }

            }

            if(Input.Keys.RIGHT == keyCode || Input.Keys.D == keyCode){
                if(camper.isLookingLeft){
                    camper.switchDirections();
                }
                camper.velocity.x = CAMPER_SPEED;
                if(camper.hasItem()){
                    camper.setState("CARRYING", false);
                }else{
                    camper.setState("WALKING", false);
                }
            }
        }

         if(Input.Keys.F == keyCode || Input.Keys.SPACE == keyCode){

             if(camper.hasItem()){
                camper.dropItem(LOWER_BOUNDS);
                if(infoBubble.isVisible()){
                    infoBubble.setVisible(false);
                }
             }else{
                 camper.setIsLooping(false);
                 camper.setState("CROUCHING", true);
                 for(Trash t:trash){
                     if(camper.canPickup(t.collider)){
                         camper.setHeldItem(t);

                         if(!hasShownCanBubble && t.getTarget() == trashCan){
                             infoBubble.setState("CAN", true);
                             infoBubble.setVisible(true);
                             hasShownCanBubble = true;
                         }else if(!hasShownFireBubble && t.getTarget() == fire){
                             infoBubble.setState("FIRE", true);
                             infoBubble.setVisible(true);
                             hasShownFireBubble = true;
                         }else if(!hasShownRiverBubble && t.getTarget() == river){
                             infoBubble.setState("RIVER", true);
                             infoBubble.setVisible(true);
                             hasShownRiverBubble = true;
                         }
                         t.setHasBeenHeld(true);
                         break;
                     }
                 }
             }
         }




        return super.keyDown(keyCode);
    }

    @Override
    public boolean keyUp(int keyCode) {
        if(!controlsEnabled){
            return false;
        }



        boolean directionKeyUp = (Input.Keys.LEFT == keyCode || Input.Keys.A == keyCode ||
                                  Input.Keys.RIGHT == keyCode || Input.Keys.D == keyCode);

        if(directionKeyUp){
            boolean leftKeyDown = (Gdx.input.isKeyPressed(Input.Keys.LEFT) ||
                    Gdx.input.isKeyPressed(Input.Keys.A));
            boolean rightKeyDown = (Gdx.input.isKeyPressed(Input.Keys.RIGHT) ||
                    Gdx.input.isKeyPressed(Input.Keys.D));
            boolean directionKeyPressed = leftKeyDown || rightKeyDown;

            if(!directionKeyPressed){
                camper.velocity.x = 0f;
            }else{
                if(leftKeyDown && camper.velocity.x > 0){
                    camper.velocity.x = -CAMPER_SPEED;
                    camper.switchDirections();
                }

                if(rightKeyDown && camper.velocity.x < 0){
                    camper.velocity.x =  CAMPER_SPEED;
                    camper.switchDirections();
                }
            }
        }


        boolean vertDirectionKeyUp = (Input.Keys.UP == keyCode || Input.Keys.W == keyCode ||
                Input.Keys.DOWN == keyCode || Input.Keys.S == keyCode);

        if(vertDirectionKeyUp){
            boolean upKeyDown = (Gdx.input.isKeyPressed(Input.Keys.UP) ||
                    Gdx.input.isKeyPressed(Input.Keys.W));
            boolean downKeyDown = (Gdx.input.isKeyPressed(Input.Keys.DOWN) ||
                    Gdx.input.isKeyPressed(Input.Keys.S));
            boolean directionKeyPressed = upKeyDown || downKeyDown;

            if(!directionKeyPressed){
                camper.velocity.y = 0f;
            }else{
                if(downKeyDown && camper.velocity.y > 0){
                    camper.velocity.y = -CAMPER_SPEED;
                }

                if(upKeyDown && camper.velocity.y < 0){
                    camper.velocity.y =  CAMPER_SPEED;
                }
            }
        }

        return super.keyUp(keyCode);
    }





    /*SHADER MAGIC*/
    private float lightSize = 1500f;
    private FrameBuffer fbo;
    private ShaderProgram finalShader;
    private ShaderProgram defaultShader;
    private TextureRegion lightRegion;

    public static final float ambientIntensity = .7f;
    public static final Vector3 ambientColor = new Vector3(0.3f, 0.3f, 0.7f);
    private static final String vertexShader = "attribute vec4 a_position;\n" +
            "attribute vec4 a_color;\n" +
            "attribute vec2 a_texCoord0;\n" +
            "uniform mat4 u_projTrans;\n" +
            "varying vec4 vColor;\n" +
            "varying vec2 vTexCoord;\n" +
            "\n" +
            "void main() {\n" +
            "\tvColor = a_color;\n" +
            "\tvTexCoord = a_texCoord0;\n" +
            "\tgl_Position = u_projTrans * a_position;\t\t\n" +
            "}";

    private static final String defaultPixelShader = "#ifdef GL_ES\n" +
            "#define LOWP lowp\n" +
            "precision mediump float;\n" +
            "#else\n" +
            "#define LOWP\n" +
            "#endif\n" +
            "\n" +
            "varying LOWP vec4 vColor;\n" +
            "varying vec2 vTexCoord;\n" +
            "\n" +
            "//our texture samplers\n" +
            "uniform sampler2D u_texture; //diffuse map\n" +
            "\n" +
            "void main() {\n" +
            "\tvec4 DiffuseColor = texture2D(u_texture, vTexCoord);\n" +
            "\tgl_FragColor = vColor * DiffuseColor;\n" +
            "}";

    private static final String finalPixelShader = "#ifdef GL_ES\n" +
            "#define LOWP lowp\n" +
            "precision mediump float;\n" +
            "#else\n" +
            "#define LOWP\n" +
            "#endif\n" +
            "\n" +
            "varying LOWP vec4 vColor;\n" +
            "varying vec2 vTexCoord;\n" +
            "\n" +
            "//texture samplers\n" +
            "uniform sampler2D u_texture; //diffuse map\n" +
            "uniform sampler2D u_lightmap;   //light map\n" +
            "\n" +
            "//additional parameters for the shader\n" +
            "uniform vec2 resolution; //resolution of screen\n" +
            "uniform LOWP vec4 ambientColor; //ambient RGB, alpha channel is intensity \n" +
            "\n" +
            "void main() {\n" +
            "\tvec4 diffuseColor = texture2D(u_texture, vTexCoord);\n" +
            "\tvec2 lighCoord = (gl_FragCoord.xy / resolution.xy);\n" +
            "\tvec4 light = texture2D(u_lightmap, lighCoord);\n" +
            "\t\n" +
            "\tvec3 ambient = ambientColor.rgb * ambientColor.a;\n" +
            "\tvec3 intensity = ambient + light.rgb;\n" +
            " \tvec3 finalColor = diffuseColor.rgb * intensity;\n" +
            "\t\n" +
            "\tgl_FragColor = vColor * vec4(finalColor, diffuseColor.a);\n" +
            "}\n";
    private void sharderStuff(){
        ShaderProgram.pedantic = false;

        defaultShader = new ShaderProgram(vertexShader, defaultPixelShader);

        finalShader = new ShaderProgram(vertexShader, finalPixelShader);
        finalShader.begin();
        finalShader.setUniformi("u_lightmap", 1);
        finalShader.setUniformf("ambientColor", ambientColor.x, ambientColor.y,
                ambientColor.z, ambientIntensity);
        finalShader.end();

        TextureAtlas atlas = gameProcessor.getAssetManager().get(AssetUtil.ANI_ATLAS, TypesUtil.TEXTURE_ATLAS);
        lightRegion = atlas.findRegion(AtlasUtil.CAMP_LIGHT);
        fireAmbience = new GenericActor((FIRE_X + fire.getWidth()/2)- ((lightRegion.getRegionHeight()*1.5f)/2),
                                        LOWER_BOUNDS - ((lightRegion.getRegionHeight()*1.5f)/2),
                                        lightRegion.getRegionWidth()*1.5f,
                                        lightRegion.getRegionHeight()*1.5f,
                                        lightRegion, Color.WHITE);
        fireAmbience.setVisible(false);
        addActor(fireAmbience);
    }

    public void resize(int width, int height) {
        fbo = new FrameBuffer(Pixmap.Format.RGBA8888, width, height, false);

        getViewport().update(width, height);
        finalShader.begin();
        finalShader.setUniformf("resolution", width, height);
        finalShader.end();
    }


    @Override
    public void draw() {

        if(isNightTime){
            Viewport vp = getViewport();
            int screenW = vp.getScreenWidth();
            int screenH = vp.getScreenHeight();
            int leftCrop = vp.getLeftGutterWidth();
            int bottomCrop = vp.getBottomGutterHeight();
            int xPos = leftCrop;
            int yPos = bottomCrop;

            fbo.begin();
            Batch batch = getBatch();
            batch.setShader(defaultShader);
            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
            Gdx.gl.glViewport(xPos, yPos, screenW, screenH);

            batch.begin();

            batch.draw(lightRegion, fireAmbience.getX(), fireAmbience.getY(), fireAmbience.getOriginX(), fireAmbience.getOriginY(),
                       fireAmbience.getWidth(), fireAmbience.getHeight(), 1.5f, 1.5f, 0f);


            batch.end();
            fbo.end();
            Gdx.gl.glViewport(xPos, yPos, screenW, screenH);

            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
            batch.setShader(finalShader);

            batch.begin();
            fbo.getColorBufferTexture().bind(1); //this is important! bind the FBO to the 2nd texture unit
            lightRegion.getTexture().bind(0); //we force the binding of a texture on first texture unit to avoid artefacts
            //this is because our default and ambiant shader dont use multi texturing...
            //youc can basically bind anything, it doesnt matter
            //batch.draw(bg, 0f, 0f);
            batch.end();
        }else{
            getBatch().setShader(defaultShader);
        }

        super.draw();
    }
}
