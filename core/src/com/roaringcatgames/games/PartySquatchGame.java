package com.roaringcatgames.games;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.kasetagen.engine.IDataSaver;
import com.kasetagen.engine.IGameProcessor;
import com.kasetagen.engine.gdx.scenes.scene2d.KasetagenStateUtil;
import com.kasetagen.engine.screen.Kitten2dScreen;
import com.kasetagen.engine.screen.LoadingScreen;
import com.roaringcatgames.games.scene2d.GameScreen;
import com.roaringcatgames.games.scene2d.IntroScreen;
import com.roaringcatgames.games.scene2d.LoseScreen;
import com.roaringcatgames.games.scene2d.WinScreen;
import com.roaringcatgames.games.utils.AssetUtil;
import com.roaringcatgames.games.utils.TypesUtil;
import com.roaringcatgames.games.utils.ViewportUtil;

public class PartySquatchGame extends Game implements IGameProcessor {

    public static final String LOADING = "loading";
    public static final String TITLE = "TITLESCREEN";
    public static final String GAME = "GAMESCREEN";
    public static final String WIN = "WINSCREEN";
    public static final String LOSE = "LOSESCREEN";


    private InputMultiplexer inputChain;
    private LoadingScreen loading;
    private Kitten2dScreen introScreen;
    private Kitten2dScreen gameScreen;
    private Kitten2dScreen winScreen;
    private Kitten2dScreen loseScreen;
    private boolean isControllerEnabled = true;
    private boolean isInitialized = false;

    protected AssetManager assetManager;


    public PartySquatchGame(){
        this.inputChain = new InputMultiplexer();
    }

	@Override
	public void create () {
        if(isControllerEnabled){
            Graphics.DisplayMode dm = Gdx.graphics.getDesktopDisplayMode();
            Gdx.app.log("DISPLAY", " Using Controller Mode. W: " + dm.width + " H: " + dm.height + " X: " + dm.bitsPerPixel);
            Gdx.graphics.setDisplayMode(dm.width, dm.height, true);
            Gdx.graphics.setVSync(true);
        }
        assetManager = new AssetManager();
        loadAssets();
	}

	@Override
	public void render () {
        if(assetManager.update()){
            if(!isInitialized){
                KasetagenStateUtil.setDebugMode(false);
                changeToScreen(TITLE);
                isInitialized = true;
            }
        }else{
            if(!isInitialized){
                changeToScreen(LOADING);
                isInitialized = true;
            }
        }

        Gdx.gl.glClearColor(0f, 0f, 0f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        super.render();
	}

    private void loadAssets(){
        assetManager.load(AssetUtil.ANI_ATLAS, TypesUtil.TEXTURE_ATLAS);
        assetManager.load(AssetUtil.REXLIA_48, TypesUtil.BITMAP_FONT);
        assetManager.load(AssetUtil.TITLE_SOUND2, TypesUtil.SOUND);
        assetManager.load(AssetUtil.CAR_DRIVING_SOUND, TypesUtil.SOUND);
        assetManager.load(AssetUtil.HIT_WOOSH1_SOUND, TypesUtil.SOUND);
        assetManager.load(AssetUtil.HIT_WOOSH2_SOUND, TypesUtil.SOUND);
        assetManager.load(AssetUtil.FIRE_CRACKLE_SOUND, TypesUtil.SOUND);
        assetManager.load(AssetUtil.WATER_DROPLET_SOUND, TypesUtil.SOUND);
        assetManager.load(AssetUtil.WIND2_SOUND, TypesUtil.SOUND);
        assetManager.load(AssetUtil.RANGER_WARNING_SOUND, TypesUtil.SOUND);
        assetManager.load(AssetUtil.FIRE_GROW_SOUND, TypesUtil.SOUND);
        assetManager.load(AssetUtil.SASQUATCH_YUB_SOUND, TypesUtil.SOUND);
        assetManager.load(AssetUtil.RANGER_EYE_SOUND, TypesUtil.SOUND);
        assetManager.load(AssetUtil.RANGER_GET_OUT2_SOUND, TypesUtil.SOUND);
        assetManager.load(AssetUtil.MOON_FART, TypesUtil.SOUND);
        assetManager.load(AssetUtil.LOGO_IMG, TypesUtil.TEXTURE);
    }

    @Override
    public String getStartScreen() {
        return TITLE;
    }

    @Override
    public boolean isLoaded() {
        return assetManager.update();
    }

    @Override
    public float getLoadingProgress() {
        return assetManager.getProgress();
    }

    @Override
    public AssetManager getAssetManager() {
        return assetManager;
    }

    @Override
    public void changeToScreen(String screenName) {

        if(LOADING.equalsIgnoreCase(screenName))    {
            if(loading == null){
                loading = new LoadingScreen(this, "animations/loading.atlas",
                        new Stage(new FitViewport(ViewportUtil.VP_WIDTH, ViewportUtil.VP_HEIGHT)));
            }

            setScreen(loading);
        }else if(TITLE.equalsIgnoreCase(screenName)){
            if(introScreen == null){
                introScreen = new IntroScreen(this);
            }

            wireGameToScreen(introScreen);
        }else if(GAME.equals(screenName)){

            if(gameScreen == null){
                gameScreen = new GameScreen(this);
            }

            wireGameToScreen(gameScreen);
        }
        else if(WIN.equals(screenName)){
            if(winScreen == null){
                winScreen = new WinScreen(this);
            }

            wireGameToScreen(winScreen);
        }
        else if(LOSE.equals(screenName)){
            if(loseScreen == null){
                loseScreen = new LoseScreen(this);
            }

            wireGameToScreen(loseScreen);
        }
    }

    @Override
    public void setBGMusic(Music music) {
        //DO NOTHING
    }

    @Override
    public void setBGMusicVolume(float v) {
        //DO NOTHING
    }


    private void wireGameToScreen(Kitten2dScreen screen){
        setScreen(screen);
        inputChain.clear();
        inputChain.addProcessor(screen);
        inputChain.addProcessor(screen.getStage());
        Gdx.input.setInputProcessor(inputChain);
    }

    @Override
    public String getStoredString(String s) {
        return null;
    }

    @Override
    public String getStoredString(String s, String s2) {
        return null;
    }

    @Override
    public int getStoredInt(String s) {
        return 0;
    }

    @Override
    public float getStoredFloat(String s) {
        return 0;
    }

    @Override
    public void saveGameData(IDataSaver iDataSaver) {

    }
}
