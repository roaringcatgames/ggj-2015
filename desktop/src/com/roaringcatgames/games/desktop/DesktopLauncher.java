package com.roaringcatgames.games.desktop;

import com.badlogic.gdx.Files;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.roaringcatgames.games.PartySquatchGame;
import com.roaringcatgames.games.utils.ViewportUtil;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.width = ViewportUtil.VP_WIDTH;
        config.height = ViewportUtil.VP_HEIGHT;
        config.title = "Party Squatch";
        config.addIcon("icon_32x32.png", Files.FileType.Internal);
		new LwjglApplication(new PartySquatchGame(), config);
	}
}
